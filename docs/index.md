# Благотворительный платеж в фонд «Волшебная ёлка»

ЮKassa теперь может принимать благотворительные платежи в фонд Деда Мороза «Волшебная ёлка». Фонд высаживает ёлки, чтобы не снижалась численность деревьев.

Чтобы воспользоваться новой функциональностью, при создании объекта платежа вам нужно дополнительно передать объект [```charity``` ](https://yootest-moxnataya-4d2f9ca6d611d76bd75b1f1f7cc19a88fef67eb135af2.gitlab.io/#charity_1). Кроме того, вы предварительно должны настроить [двухстадийные платежи](https://yookassa.ru/developers/payment-acceptance/getting-started/payment-process#capture-and-cancel).
Рассмотрим этот процесс подробнее.

##### Список параметров объекта charity

<table>
  <tr>
    <th>Параметр</th>
    <th>Описание</th>
  </tr>
  <tr>
    <td>charity <br> <span style="color: grey;">object</span></td>
    <td>Сопутствующие перечисления на  
благотворительность.
</td>
  </tr>
  <tr>
    <td>charity.type <br> <span style="color: grey;">string</span>
</td>
    <td>Тип платежа. <br> Фиксированное значение: <b>chistmas_tree<b></td>
  </tr>
  </td>
  </tr>
  <tr>
    <td>charity.donator  <br> <span style="color: grey;">string</span>
</td>
    <td>Имя пользователя, сделавшего взнос. Например: <b>Иванна Иванова</b>. <br> Это необязательнй параметр, пользователь вводит имя только, если хочет, чтобы его опубликовали на сайте фонда.
</td>
  </tr>
  <tr>
    <td>сharity.donation   <br> <span style="color: grey;">object</span></td>
    <td>Данные о пожертвовании.</td>
  </tr>
  <tr>
    <td>charity.donation.type   <br> <span style="color: grey;">string</span></td>
    <td>Тип пожертвования. Возможны два значения: <b>money</b> или <b>tree</b>.<br> Пользователь выбирает <b>money</b>, если хочет пожертвовать произвольную сумму, а — <b>tree</b>, если хочет помочь высадить конкретное количество деревьев.
</td>
  </tr>
  <tr>
    <td>charity.donation.quantity <br> <span style="color: grey;">number</span></td>
    <td>Количество деревьев. Например: <b>1</b> <br> Параметр нужно указать, если выбрано значение <b>tree</b>. 
</td>
  </tr>
  <tr>
    <td>charity.donation.amount <br> <span style="color: grey;">object</span></td>
    <td>Размер пожертвования. Если выбрано значение <b>tree</b>, система рассчитает конечную сумму на основании параметра <b>quantity</b> исходя из стоимости одного дерева. Одно дерево стоит 100 рублей. Пример объекта для значения <b>tree</b> приводим <a href='https://yootest-moxnataya-4d2f9ca6d611d76bd75b1f1f7cc19a88fef67eb135af2.gitlab.io/#charity-tree'>тут</a>. <br> При выборе значения <b>money</b> пользователь указывает произвольную сумму, которую хочет пожертвовать. Пример объекта для значения <b>money</b> приводим <a href='https://yootest-moxnataya-4d2f9ca6d611d76bd75b1f1f7cc19a88fef67eb135af2.gitlab.io/#charity-money'>тут</a>.
</td>
  </tr>
  <tr>
    <td>charity.donation.amount.value <br> <span style="color: grey;">string</span></td>
    <td>Сумма в выбранной валюте. Разделителем целой и дробной части суммы служит точка. Количество знаков после точки зависит от выбранной валюты. Например: <b>10.00</b>  
</td>
  </tr>
  <tr>
    <td>charity.donation.amount.currency <br> <span style="color: grey;">string</span></td>
    <td>Код валюты в формате <a href='https://en.wikipedia.org/wiki/ISO_4217#List_of_ISO_4217_currency_codes' target="_blank">ISO-4217</a>. Например: <b>RUB</b>
</table>

##### Пример созданного объекта платежа charity со значением tree
```json
{
  "charity": { 
    "type": "christmas_tree",
    "donator": "Петр Петров",
    "donation": {
      "type": "tree",
      "quantity": 5,
      "amount": {
        "value": "500.00",
        "currency": "RUB"
      }
    }
  }
```

##### Пример созданного объекта платежа charity со значением money
```json
{
  "charity": { 
    "type": "christmas_tree",
    "donator": "Петр Петров",
    "donation": {
      "type": "money",
      "amount": {
        "value": "550.00",
        "currency": "RUB"
      }
    }
  }
```

Чтобы принять благотворительный платеж, нужно выполнить три шага.

#### Шаг 1. Создание платежа

Чтобы создать двухстадийный платеж, передайте в запросе на [создание платежа ](https://yookassa.ru/developers/api#create_payment) параметр `capture` со значением `false`.
Такой платеж будет доступен при оплате банковской картой на стороне ЮMoney.

##### Пример запроса на создание двухстадийного платежа

```python
from yookassa import Configuration, Payment

Configuration.account_id = <Идентификатор магазина>
Configuration.secret_key = <Секретный ключ>

payment = Payment.create({
    "amount": {
        "value": "100.00",
        "currency": "RUB"
    },
    "confirmation": {
        "type": "redirect",
        "return_url": "https://www.example.com/return_url"
    },
    "capture": False,
    "description": "Заказ 37",
    "metadata": {
      "order_id": "37"
    }
})
```

#### Шаг 2. Перенаправление на пользователя на страницу ЮКаssa
Перенаправьте пользователя на страницу ЮKassa, ссылка на нее придет в параметре ```confirmation_url```. На этой странице пользователь введет данные карты и подтвердит платеж.


#### Шаг 3. Оплата банковской картой
Дальше следуйте процессу [оплаты банковской картой](https://yookassa.ru/developers/payment-acceptance/integration-scenarios/manual-integration/bank-card?codeLang=python), начиная с **Шага 3**.
